"""
Django App Config for Django Tidyfields
"""
from django.apps import AppConfig


class DjangoTidyfieldsConfig(AppConfig):
    """
    Initialize app with base parameters.
    """
    name = 'django_tidyfields'
