.. django_tidyfields documentation master file, created by startproject.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django_tidyfields's documentation!
=================================================

|build-status| |requirements| |coverage|

|python-versions| |django-versions| |pypi-version|

<One liner describing the project>

Features
========

* ...
* ...

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. |build-status| image:: https://gitlab.com/routhio/django-tidyfields/badges/master/pipeline.svg
    :target: https://gitlab.com/routhio/django-tidyfields/commits/master

.. |requirements| image:: https://requires.io/enterprise/Routhinator/django_tidyfields/requirements.svg?branch=master
    :target: https://requires.io/enterprise/Routhinator/django_tidyfields/requirements/?branch=master
    :alt: Requirements status

.. |coverage| image:: https://gitlab.com/routhio/django-tidyfields/badges/master/coverage.svg
    :target: https://gitlab.com/routhio/python/django-tidyfields/commits/master
    :alt: Coverage status

.. |python-versions| image:: https://img.shields.io/pypi/pyversions/django_tidyfields.svg

.. |django-versions| image:: https://img.shields.io/pypi/djversions/django_tidyfields.svg

.. |pypi-version| image:: https://img.shields.io/pypi/v/django_tidyfields.svg
