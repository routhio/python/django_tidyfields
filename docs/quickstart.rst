==========
Quickstart
==========

Installation
============

Install from PyPI with pip:

.. code-block:: bash

    pip install django_tidyfields


Usage
=====

<document how to use the app here>
